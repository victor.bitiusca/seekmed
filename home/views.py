from django.shortcuts import render
from django.views.generic import TemplateView


class HomeTemplateView(TemplateView):
    template_name = "home/home.html"


class ServiceTemplateView(TemplateView):
    template_name = 'servicii/servicii.html'


class MachineTemplateView(TemplateView):
    template_name = 'machines/aparate.html'


class ReconTemplateView(TemplateView):
    template_name = 'reconditionings/reconditionari.html'


class ContactTemplateView(TemplateView):
    template_name = 'contact/contact.html'


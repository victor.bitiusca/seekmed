from django.urls import path
from home import views

urlpatterns = [
    path("", views.HomeTemplateView.as_view(), name="home"),
    path("servicii/", views.ServiceTemplateView.as_view(), name="servicii"),
    path("machines/", views.MachineTemplateView.as_view(), name="aparate"),
    path("reconditionings/", views.ReconTemplateView.as_view(), name="reconditionari"),
    path("contact/", views.ContactTemplateView.as_view(), name="contact"),

]
